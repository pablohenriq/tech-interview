const withPlugins = require('next-compose-plugins')
const withFonts = require('next-fonts')
const withSass = require('@zeit/next-sass')

module.exports = withPlugins([withSass, withFonts])
